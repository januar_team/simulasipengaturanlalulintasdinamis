﻿using AForge;
using AForge.Imaging;
using AForge.Imaging.Filters;
using Newtonsoft.Json;
using SimulasiPengaturanLaluLintasDinamis.Neuro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SimulasiPengaturanLaluLintasDinamis
{
    public partial class FormUtama : Form
    {
        private ANFIS anfis;

        private Bitmap originalImage;
        private Bitmap roiImage;
        private Bitmap citraUji;
        private Bitmap morfologi;

        private bool isStart = false;
        private int activeStreet = 0;
        Thread threadStart;

        /* Object line untuk menentukan garis tepi jalan yg digunakan pada proses hough transform(hough line) 
         * Class : PointLine.cs
         */
        PointLine leftLine = null;
        PointLine rightLine = null;

        int TotalPixel = 0;
        int PixelKendaraan = 0;
        int SleepTime = 0;

        // filter untuk grayscale
        private FiltersSequence filter = new FiltersSequence(
            Grayscale.CommonAlgorithms.BT709,
            new Threshold(64)
        );

        // filter untuk morfology
        private FiltersSequence morfologiFilter = new FiltersSequence(
            new Opening(),
            new Closing()
        );

        ConnectedComponentsLabeling cclFilter;

        public FormUtama()
        {
            InitializeComponent();

            // menghapus temporary file pada proses simulasi sebelumnya
            Assembly assembly = this.GetType().Assembly;
            string path = System.IO.Path.GetDirectoryName(assembly.Location) + "\\temp\\";
            DirectoryInfo dic = new DirectoryInfo(path);
            if (!dic.Exists)
            {
                dic.Create();
            }
            else
            {
                foreach (FileInfo file in dic.GetFiles())
                {
                    file.Delete();
                }
            }

            initVlc();
        }

        private void VlcControl_VlcLibDirectoryNeeded(object sender, Vlc.DotNet.Forms.VlcLibDirectoryNeededEventArgs e)
        {
            try
            {
                var currentAssembly = Assembly.GetEntryAssembly();
                var currentDirectory = new FileInfo(currentAssembly.Location).DirectoryName;
                if (currentDirectory == null)
                    return;
                if (AssemblyName.GetAssemblyName(currentAssembly.Location).ProcessorArchitecture == ProcessorArchitecture.X86)
                    e.VlcLibDirectory = new DirectoryInfo(System.IO.Path.Combine(currentDirectory, @"lib\x86\"));
                else
                    e.VlcLibDirectory = new DirectoryInfo(System.IO.Path.Combine(currentDirectory, @"lib\x64\"));
            }
            catch (Exception ex)
            {
                
                General.Error(ex.Message);
            }
        }

        private void initVlc() {
            //vlcControl.VlcLibDirectoryNeeded += VlcControl_VlcLibDirectoryNeeded;
            //vlcControl.EndInit();
            var mediaOptions = new string[] { "input-repeat=-1" };

            vlcControl1.SetMedia(new FileInfo(@"data\video3.mp4"), mediaOptions);
            vlcControl1.Play();
            vlcControl1.Audio.IsMute = true;

            vlcControl2.SetMedia(new FileInfo(@"data\video2.mp4"), mediaOptions);
            vlcControl2.Play();
            vlcControl2.Audio.IsMute = true;

            vlcControl3.SetMedia(new FileInfo(@"data\video.mp4"), mediaOptions);
            vlcControl3.Play();
            vlcControl3.Audio.IsMute = true;

            vlcControl4.SetMedia(new FileInfo(@"data\video3.mp4"), mediaOptions);
            vlcControl4.Play();
            vlcControl4.Time = 15000;
            vlcControl4.Audio.IsMute = true;

            Thread.Sleep(100);
            vlcControl1.Pause();
            vlcControl2.Pause();
            vlcControl3.Pause();
            vlcControl4.Pause();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (isStart)
            {
                btnStart.Text = "Start";
                isStart = false;
                vlcControl1.Pause();
                vlcControl2.Pause();
                vlcControl3.Pause();
                vlcControl4.Pause();
            }
            else
            {
                double lebar_jalan = 0;

                if (anfis == null)
                {
                    General.Error("Please insert traning data!");
                }
                else if (txtLebarJalan.Text == "")
                {
                    General.Error("Please enter street width!");
                }
                else if (!Double.TryParse(txtLebarJalan.Text, out lebar_jalan))
                {
                    General.Error("Street width not valid!");
                }
                else
                {
                    btnStart.Text = "Stop";
                    lblTime.Text = "0";
                    lblKepadatanJalan.Text = "0%";
                    isStart = true;
                    vlcControl1.Play();
                    vlcControl2.Play();
                    vlcControl3.Play();
                    vlcControl4.Play();

                    threadStart = new Thread(new ThreadStart(() =>
                    {

                        Thread.Sleep(4000);
                        while (isStart)
                        {
                            //change street lamp
                            if (activeStreet == 0)
                            {
                                pctGreen1.Image = Properties.Resources.green;
                                pctRed1.Image = Properties.Resources.black;
                                activeStreet = 1;
                            }
                            else if(activeStreet == 1)
                            {
                                pctGreen1.Image = Properties.Resources.black;
                                pctYellow1.Image = Properties.Resources.yellow;
                                pctYellow2.Image = Properties.Resources.yellow;
                                Thread.Sleep(1000);

                                pctYellow1.Image = Properties.Resources.black;
                                pctYellow2.Image = Properties.Resources.black;

                                pctRed1.Image = Properties.Resources.red;
                                pctGreen2.Image = Properties.Resources.green;
                                activeStreet = 2;
                            }
                            else if (activeStreet == 2)
                            {
                                pctGreen2.Image = Properties.Resources.black;
                                pctYellow2.Image = Properties.Resources.yellow;
                                pctYellow3.Image = Properties.Resources.yellow;
                                Thread.Sleep(1000);

                                pctYellow2.Image = Properties.Resources.black;
                                pctYellow3.Image = Properties.Resources.black;

                                pctRed2.Image = Properties.Resources.red;
                                pctGreen3.Image = Properties.Resources.green;
                                activeStreet = 3;
                            }
                            else if (activeStreet == 3)
                            {
                                pctGreen3.Image = Properties.Resources.black;
                                pctYellow3.Image = Properties.Resources.yellow;
                                pctYellow4.Image = Properties.Resources.yellow;
                                Thread.Sleep(1000);

                                pctYellow3.Image = Properties.Resources.black;
                                pctYellow4.Image = Properties.Resources.black;

                                pctRed3.Image = Properties.Resources.red;
                                pctGreen4.Image = Properties.Resources.green;
                                activeStreet = 4;
                            }
                            else if (activeStreet == 4)
                            {
                                pctGreen4.Image = Properties.Resources.black;
                                pctYellow4.Image = Properties.Resources.yellow;
                                pctYellow1.Image = Properties.Resources.yellow;
                                Thread.Sleep(1000);

                                pctYellow4.Image = Properties.Resources.black;
                                pctYellow1.Image = Properties.Resources.black;

                                pctRed4.Image = Properties.Resources.red;
                                pctGreen1.Image = Properties.Resources.green;
                                activeStreet = 1;
                            }


                            CarDetection();
                            Thread.Sleep(500);
                            List<Neuro.Clustering.DataItem> data = new List<Neuro.Clustering.DataItem>();
                            double kepadatanJalan = ((double)PixelKendaraan / TotalPixel) * 100;
                            data.Add(new Neuro.Clustering.DataItem()
                            {
                                Data = new double[] { kepadatanJalan, lebar_jalan }
                            });
                            anfis.sb = new StringBuilder();
                            var result = anfis.Execute(data);

                            
                            double time = Math.Round(result[0]);
                            try
                            {
                                this.BeginInvoke(new Action(() =>
                                {
                                    txtDetail.Text += "\r\n" + anfis.sb.ToString();
                                    lblKepadatanJalan.Text = String.Format("{0} %", Math.Round(kepadatanJalan, 2));
                                    lblPixelJalan.Text = TotalPixel.ToString();
                                    lblPixelKendaraan.Text = PixelKendaraan.ToString();
                                    lblSimpang.Text = (activeStreet).ToString();
                                    txtDetail.SelectionStart = txtDetail.TextLength;
                                    txtDetail.ScrollToCaret();
                                }));
                            }catch(Exception ex) { }

                            for (int i = (int)time; i >= 0; i--)
                            {
                                try
                                {
                                    this.BeginInvoke(new Action(() =>
                                    {
                                        lblTime.Text = i.ToString();
                                    }));
                                }catch(Exception ex) { }
                                
                                Thread.Sleep(1000);
                                if (!isStart) break;
                            }
                            
                        }
                    }));
                    threadStart.IsBackground = true;
                    threadStart.Start();
                }
            }
        }

        #region Pre Processing
        ///<summary>
        /// This function to start preprocessing process of ROI image (Region of Interest). This process will start step by step
        /// to identify part of street.
        ///</summary>
        private void PreProcessing(String filename, double realtive_intensity)
        {
            originalImage = (Bitmap)Bitmap.FromFile(filename);

            Bitmap segmentation = Segmentation(originalImage);

            Bitmap canny = EdgeDetection(segmentation);

            roiImage = HoughTransformation(canny, originalImage, realtive_intensity);

            pctRoi.Image = originalImage;
            pctSegmentasi.Image = segmentation;
            pctEdge.Image = canny;
            pctElimination.Image = roiImage;
        }

        ///<summary>
        /// Start segmentation process. This process will identify gray area of pixel color. If it gray color will change 
        /// to black otherwise will set white
        ///</summary>
        ///<param name="image">Bitmap image will be segmentated</param>
        private Bitmap Segmentation(Bitmap image)
        {
            Bitmap temp = (Bitmap)image.Clone();
            Color c;

            for (int i = 0; i < temp.Width; i++)
            {
                for (int j = 0; j < temp.Height; j++)
                {
                    c = temp.GetPixel(i, j);
                    if ((c.R >= 65 && c.R <= 190) && (c.G >= 70 && c.G <= 190) && (c.B >= 80 && c.B <= 190))
                    {
                        temp.SetPixel(i, j, Color.Black);
                    }
                    else
                    {
                        temp.SetPixel(i, j, Color.White);
                    }
                }
            }

            return temp;
        }

        private Bitmap EdgeDetection(Bitmap image)
        {
            AForge.Imaging.Filters.CannyEdgeDetector cannyFilter = new AForge.Imaging.Filters.CannyEdgeDetector();
            Bitmap cannyImage = cannyFilter.Apply(AForge.Imaging.Filters.Grayscale.CommonAlgorithms.BT709.Apply(image));

            return cannyImage;
        }

        private Bitmap HoughTransformation(Bitmap image, Bitmap originalImage, double realtive_intensity)
        {
            leftLine = null;
            rightLine = null;
            HoughLineTransformation houghLineTransform = new HoughLineTransformation();

            Bitmap temp = AForge.Imaging.Image.Clone(image, PixelFormat.Format24bppRgb);
            Bitmap clone = (Bitmap)temp.Clone();
            /// lock the source image
            BitmapData sourceData = temp.LockBits(
                new Rectangle(0, 0, temp.Width, temp.Height),
                ImageLockMode.ReadOnly, temp.PixelFormat);
            // binarize the image
            UnmanagedImage binarySource = filter.Apply(new UnmanagedImage(sourceData));
            BitmapData cloneSourceData = clone.LockBits(
                new Rectangle(0, 0, clone.Width, clone.Height),
                ImageLockMode.ReadOnly, clone.PixelFormat);
            // apply Hough line transofrm
            houghLineTransform.ProcessImage(binarySource);

            // get lines using relative intensity
            HoughLine[] lines = houghLineTransform.GetLinesByRelativeIntensity(realtive_intensity);

            foreach (HoughLine line in lines)
            {

                if (line.Theta > 50 && line.Theta < 130)
                {
                    continue;
                }

                string s = string.Format("Theta = {0}, R = {1}, I = {2} ({3})", line.Theta, line.Radius, line.Intensity, line.RelativeIntensity);
                System.Diagnostics.Debug.WriteLine(s);

                // uncomment to highlight detected lines

                // get line's radius and theta values
                int r = line.Radius;
                double t = line.Theta;

                // check if line is in lower part of the image
                if (r < 0)
                {
                    t += 180;
                    r = -r;
                }

                // convert degrees to radians
                t = (t / 180) * Math.PI;

                // get image centers (all coordinate are measured relative
                // to center)
                int w2 = image.Width / 2;
                int h2 = image.Height / 2;

                double x0 = 0, x1 = 0, y0 = 0, y1 = 0;

                if (line.Theta != 0)
                {
                    // none vertical line
                    x0 = -w2; // most left point
                    x1 = w2;  // most right point

                    // calculate corresponding y values
                    y0 = (-Math.Cos(t) * x0 + r) / Math.Sin(t);
                    y1 = (-Math.Cos(t) * x1 + r) / Math.Sin(t);
                }
                else
                {
                    // vertical line
                    x0 = line.Radius;
                    x1 = line.Radius;

                    y0 = h2;
                    y1 = -h2;
                }

                // draw line on the image
                if (leftLine == null || rightLine == null)
                {
                    // menentukan garis tepi yang digunakan pada jalan
                    if (line.Theta >= 25 && line.Theta < 45) // garis tepi kiri jalan
                    {
                        if (leftLine != null) continue;
                        leftLine = new PointLine();
                        leftLine.Point1 = new IntPoint((int)x0 + w2, h2 - (int)y0);
                        leftLine.Point2 = new IntPoint((int)x1 + w2, h2 - (int)y1);

                        drawLine(ref sourceData, x0, x1, y0, y1, h2, w2);
                    }
                    else if (line.Theta > 130) // garis tepi kanan jalan
                    {
                        if (rightLine != null) continue;
                        rightLine = new PointLine();
                        rightLine.Point1 = new IntPoint((int)x0 + w2, h2 - (int)y0 + 10);
                        rightLine.Point2 = new IntPoint((int)x1 + w2, h2 - (int)y1);

                        drawLine(ref sourceData, x0, x1, y0, y1, h2, w2);
                    }
                }
                //drawLine(ref cloneSourceData, x0, x1, y0, y1, h2, w2);
            }

            System.Diagnostics.Debug.WriteLine("Found lines: " + houghLineTransform.LinesCount);
            System.Diagnostics.Debug.WriteLine("Max intensity: " + houghLineTransform.MaxIntensity);
            System.Diagnostics.Debug.WriteLine("Houghline: " + lines.Length);

            // menghitung banyaknya pixel yang manjadi daerah jalan dan mengextraki daerah jalan yang digunakan
            Bitmap roi = (Bitmap)originalImage.Clone();
            TotalPixel = 0;
            for (int x = 0; x < roi.Width; x++)
            {
                for (int y = 0; y < roi.Height; y++)
                {
                    if (((y * (leftLine.Point2.X - leftLine.Point1.X) - x * (leftLine.Point2.Y - leftLine.Point1.Y)) >
                        (leftLine.Point1.Y * (leftLine.Point2.X - leftLine.Point1.X) - leftLine.Point1.X * (leftLine.Point2.Y - leftLine.Point1.Y))) &&
                        (y * (rightLine.Point2.X - rightLine.Point1.X) - x * (rightLine.Point2.Y - rightLine.Point1.Y)) >
                        (rightLine.Point1.Y * (rightLine.Point2.X - rightLine.Point1.X) - rightLine.Point1.X * (rightLine.Point2.Y - rightLine.Point1.Y)))
                    {
                        // nothing
                        TotalPixel++;
                    }
                    else
                    {
                        roi.SetPixel(x, y, Color.Black);
                    }
                }
            }

            // unlock source image
            temp.UnlockBits(sourceData);
            // dispose temporary binary source image
            binarySource.Dispose();
            pctHoughSelection.Image = temp;

            clone.UnlockBits(cloneSourceData);
            pctHough.Image = clone;


            return roi;
        }

        private void drawLine(ref BitmapData sourceData, double x0, double x1, double y0, double y1, int h2, int w2)
        {
            Drawing.Line(sourceData,
                        new IntPoint((int)x0 + w2, h2 - (int)y0),
                        new IntPoint((int)x1 + w2, h2 - (int)y1),
                        Color.Red);
            System.Diagnostics.Debug.WriteLine(String.Format("Point ({0},{1}),({2},{3})", (int)x0 + w2, h2 - (int)y0, (int)x1 + w2, h2 - (int)y1));
        }
        #endregion


        #region Car Detection
        private void CarDetection(string filename = "")
        {
            if (filename == "")
            {
                System.Threading.Thread.Sleep(SleepTime);
                Assembly assembly = this.GetType().Assembly;
                string path = System.IO.Path.GetDirectoryName(assembly.Location) + "\\temp\\";

                if (!File.Exists(path))
                    Directory.CreateDirectory(path);

                // melakukan pengambilan gambar pada video yang diputar
                // file akan disimpan pada folder temp
                filename = path + DateTime.Now.ToString("yyyy-MM-dd HH mm ss") + ".png";
                if(activeStreet == 1 || activeStreet == 4)
                {
                    //PreProcessing(@"data\roi.png", 0.35);
                    PreProcessing(@"data\roi3.png", 0.2);
                    vlcControl1.TakeSnapshot(filename);
                }else if(activeStreet == 2)
                {
                    PreProcessing(@"data\roi2.png", 0.2);
                    vlcControl2.TakeSnapshot(filename);
                }else if(activeStreet == 3)
                {
                    //PreProcessing(@"data\roi3.png", 0.2);
                    PreProcessing(@"data\roi.png", 0.35);
                    vlcControl3.TakeSnapshot(filename);
                }else if(activeStreet == 4)
                {
                    PreProcessing(@"data\roi.png", 0.35);
                    vlcControl4.TakeSnapshot(filename);
                }
                System.Threading.Thread.Sleep(1000);
            }

            Bitmap snapshoot = (Bitmap)Bitmap.FromFile(filename);
            // create filter
            ResizeBicubic filterResize = new ResizeBicubic(roiImage.Width, roiImage.Height);
            // apply the filter
            citraUji = filterResize.Apply(snapshoot);

            Bitmap subtraksi = Substraksi(roiImage, citraUji);
            Bitmap filterGT = filter.Apply(subtraksi);

            morfologi = new Opening().Apply(new Closing().Apply(filterGT));

            // melakukan proses ConnectedComponentsLabeling
            cclFilter = new ConnectedComponentsLabeling();
            cclFilter.MinHeight = 50000;
            cclFilter.MinWidth = 50000;
            Bitmap ccl = cclFilter.Apply(morfologi);

            Console.WriteLine(cclFilter.ObjectCount);

            pctUji.Image = citraUji;
            pctSubstraksi.Image = subtraksi;
            pctGT.Image = filterGT;
            pctMorfologi.Image = morfologi;
            pctCCL.Image = ccl;
            Console.WriteLine(String.Format("roi width : {0}, roi height : {1}, ccl width : {2}, ccl height {3}", roiImage.Width, roiImage.Height, ccl.Width, ccl.Height));
            // dilakukan penghitungan dan deteksi kendaraan 
            // pada saat event Paint Component PictureBox
            pctResult.Image = citraUji;
        }

        ///<summary>
        /// Fungsi untuk melakukan subtraksi hasil bitmap deteksi jalan dengan bitmap 
        /// yang akan diuji/diproses untuk mendeteksi kendaraan
        /// Pada proses subtraksi, yang dilakukan adalah proses pengurangan antara dua citra 
        /// yaitu citra ROI hasil deteksi jalan dengan citra uji
        /// </summary>
        public Bitmap Substraksi(Bitmap roiImage, Bitmap ujiImage)
        {
            Bitmap temp = (Bitmap)ujiImage.Clone();
            for (int y = 0; y < roiImage.Height; y++)
            {
                for (int x = 0; x < roiImage.Width; x++)
                {
                    Color roiPixel = roiImage.GetPixel(x, y);
                    Color ujiPixel = ujiImage.GetPixel(x, y);
                    Color newColor;
                    // pengurangan pixel RGB
                    int red = roiPixel.R - ujiPixel.R;
                    int green = roiPixel.G - ujiPixel.G;
                    int blue = roiPixel.B - ujiPixel.B;

                    // pengurangan citra hanya akan dilakukan jika posisi pixel pada daerah jalan hasil deteksi jalan
                    if (((y * (leftLine.Point2.X - leftLine.Point1.X) - x * (leftLine.Point2.Y - leftLine.Point1.Y)) >
                        (leftLine.Point1.Y * (leftLine.Point2.X - leftLine.Point1.X) - leftLine.Point1.X * (leftLine.Point2.Y - leftLine.Point1.Y))) &&
                        (y * (rightLine.Point2.X - rightLine.Point1.X) - x * (rightLine.Point2.Y - rightLine.Point1.Y)) >
                        (rightLine.Point1.Y * (rightLine.Point2.X - rightLine.Point1.X) - rightLine.Point1.X * (rightLine.Point2.Y - rightLine.Point1.Y)))
                    {
                        if (red < 0 && red >= -50)
                        {
                            red = 0;
                        }
                        else
                        {
                            red = Math.Abs(red) + 30;
                            red = (red > 255) ? 255 : red;
                        }

                        if (green < 0 && green >= -50)
                        {
                            green = 0;
                        }
                        else
                        {
                            green = Math.Abs(green) + 30;
                            green = (green > 255) ? 255 : green;
                        }

                        if (blue < 0 && blue >= -50)
                        {
                            blue = 0;
                        }
                        else
                        {
                            blue = Math.Abs(blue) + 30;
                            blue = (blue > 255) ? 255 : blue;
                        }

                        newColor = Color.FromArgb(red, green, blue);

                        temp.SetPixel(x, y, newColor);
                    }
                    else // jika tidak pada daerah jalan, pixel diset black
                    {
                        temp.SetPixel(x, y, Color.Black);
                    }
                }
            }

            return temp;
        }

        /*
         * Fungsi untuk melakukan evaluasi hasil deteksi
         * Hasil deteksi yang saling berdekatan dengan jarak citra tertentu (5 pixel)
         * akan digabungkan menjadi hasil hasil deteksi
         */
        private List<Rectangle> EvaluationNear(List<Rectangle> rect)
        {
            List<Rectangle> result = new List<Rectangle>();
            int c = 5;

            foreach (Rectangle item1 in rect)
            {
                bool add = true;
                bool check1 = false;
                int index = 0;

                foreach (Rectangle item2 in result)
                {

                    //check kanan dari object yg sudah disimpan
                    if (item2.X < item1.X)
                    {
                        if (new Rectangle(item2.X - c, item2.Y - c, item2.Width + 2 * c, item2.Height + 2 * c).Contains(item1.X, item1.Y))
                        {
                            add = false;
                            check1 = true;
                            break;
                        }
                    }
                    else
                    {
                        if (new Rectangle(item1.X - c, item1.Y - c, item1.Width + c, item1.Height + c).Contains(item2.X, item2.Y + item2.Height))
                        {
                            add = false;
                            check1 = true;
                            break;
                        }
                    }
                    index++;
                }

                if (check1)
                {
                    Rectangle temp = new Rectangle(result[index].X, result[index].Y, (item1.X - result[index].X) + item1.Width, (item1.Y - result[index].Y) + item1.Height);
                    result[index] = temp;
                }
                else if (add)
                {
                    result.Add(item1);
                }

            }

            return result;
        }
        #endregion

        /*
         * Event untuk menampilkan hasil deteksi kendaraan
         * Pada saat menampilkan citra akan dilakukan proses evaluasi dan penghitugan kendaraan
         */
        private void pctResult_Paint(object sender, PaintEventArgs e)
        {
            if (morfologi != null)
            {
                BlobCounter bc = (BlobCounter)cclFilter.BlobCounter;
                //bc.ProcessImage(morfologi);
                Rectangle[] rects = bc.GetObjectsRectangles();
                List<Rectangle> evaluation = new List<Rectangle>();
                int jumlah = 0;

                /*
                 * Evaluasi pertama hasil deteksi
                 * menghapus hasil deteksi yang saling bertindih
                 */
                foreach (Rectangle rect in rects)
                {
                    if (rect.Width > 5 && rect.Height > 5)
                    {
                        bool cek1 = false;
                        bool cek2 = true;
                        int index = 0;
                        foreach (Rectangle recEva in evaluation)
                        {
                            if (recEva.Contains(rect))
                            {
                                cek2 = false;
                                break;
                            }
                            if (rect.Contains(recEva))
                            {
                                cek1 = true;
                                cek2 = false;
                                break;
                            }
                            index++;
                        }

                        if (cek1)
                        {
                            evaluation[index] = rect;
                        }
                        else if (cek2)
                        {
                            evaluation.Add(rect);
                        }
                    }
                    // evaluation.Add(rect);
                }

                /*
                 * Evaluasi kedua hasil deteksi
                 * Menggabungkan hasil deteksi yang berdekatan
                 */
                // evaluation = EvaluationNear(evaluation);

                // menampilkan dan menghitung kendaraan hasil deteksi
                PixelKendaraan = 0;
                foreach (Rectangle rect in evaluation)
                {
                    Pen pen = new Pen(Color.Red, 2);
                    float X = (float)pctResult.Width / roiImage.Width;
                    float Y = (float)pctResult.Height / roiImage.Height;
                    Rectangle newRec = new Rectangle((int)Math.Ceiling(rect.X * X), (int)Math.Ceiling(rect.Y * Y),
                        (int)Math.Ceiling(rect.Width * X), (int)Math.Ceiling(rect.Height * Y));
                    e.Graphics.DrawRectangle(pen, newRec);
                    PixelKendaraan += (rect.Width * rect.Height);
                    jumlah++;
                }
                //lblJumlahKendaraan.Text = jumlah.ToString();
                Console.WriteLine("Total Pixel" + TotalPixel);
                Console.WriteLine("Pixel Kendaraan" + PixelKendaraan);
                //float percentage = float.Parse(Math.Round(((decimal)pixelKendaraan / TotalPixel) * 100, 2).ToString());
                //lblKepadatan.Text = String.Format("{0} %", percentage);

                // melakukan perhitungan fuzzy dari hasil deteksi kendaraan
                //FuzzyObject fuzzy = new FuzzyObject();
                //lblSepi.Text = Math.Round(fuzzy.lvKepadatanJalan.GetLabelMembership("Sepi", percentage), 2).ToString();
                //lblSedang.Text = Math.Round(fuzzy.lvKepadatanJalan.GetLabelMembership("Sedang", percentage), 2).ToString();
                //lblPadat.Text = Math.Round(fuzzy.lvKepadatanJalan.GetLabelMembership("Padat", percentage), 2).ToString();
            }
        }

        private void trainingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormTraning frmTraning = new FormTraning();
            frmTraning.ShowDialog();
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            DialogResult dialog = openFileDialog1.ShowDialog();
            if(dialog == DialogResult.OK)
            {
                using(StreamReader sr = new StreamReader(openFileDialog1.FileName))
                {
                    string traning_data = sr.ReadToEnd();
                    TraningData traningData = JsonConvert.DeserializeObject<TraningData>(traning_data);
                    txtDetail.Text = JsonConvert.SerializeObject(traningData, Formatting.Indented);
                    anfis = new ANFIS(traningData);
                }
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            txtTraningFile.Text = openFileDialog1.FileName;
        }

        private void FormUtama_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.FormOwnerClosing)
                this.Owner.Close();
        }
    }
}
