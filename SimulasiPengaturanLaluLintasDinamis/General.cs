﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimulasiPengaturanLaluLintasDinamis
{
    class General
    {
        public static int ColumnWidth = 20;
        public static int TotalColumn = 5;

        public static void Error(String message) {
            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void Success(String message)
        {
            MessageBox.Show(message, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void ShowError(String message)
        {
            System.Windows.Forms.MessageBox.Show(message, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
        }

        public static String DrawLine()
        {
            StringBuilder temp = new StringBuilder();
            temp.Append(new string('-', TotalColumn * ColumnWidth + (TotalColumn + 1)));
            temp.Append("\r\n");
            return temp.ToString();
        }

        public static String DrawRow(string[] label)
        {
            StringBuilder temp = new StringBuilder();
            for (int i = 0; i < label.Length; i++)
            {
                int width = ColumnWidth;
                temp.Append("|");
                if (string.IsNullOrEmpty(label[i]))
                {
                    temp.Append(new string(' ', width));
                }
                else
                {
                    temp.Append(label[i].PadRight(width - (width - label[i].Length) / 2)
                        .PadLeft(width));
                }
            }
            temp.Append("|");
            temp.Append("\r\n");
            return temp.ToString();
        }
    }
}
