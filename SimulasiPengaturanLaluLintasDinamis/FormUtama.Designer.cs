﻿namespace SimulasiPengaturanLaluLintasDinamis
{
    partial class FormUtama
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUtama));
            this.vlcControl1 = new Vlc.DotNet.Forms.VlcControl();
            this.btnStart = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDetail = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.pctResult = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pctUji = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pctCCL = new System.Windows.Forms.PictureBox();
            this.pctMorfologi = new System.Windows.Forms.PictureBox();
            this.pctGT = new System.Windows.Forms.PictureBox();
            this.pctSubstraksi = new System.Windows.Forms.PictureBox();
            this.pctElimination = new System.Windows.Forms.PictureBox();
            this.pctHoughSelection = new System.Windows.Forms.PictureBox();
            this.pctHough = new System.Windows.Forms.PictureBox();
            this.pctEdge = new System.Windows.Forms.PictureBox();
            this.pctSegmentasi = new System.Windows.Forms.PictureBox();
            this.pctRoi = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trainingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txtTraningFile = new System.Windows.Forms.TextBox();
            this.pctRed1 = new System.Windows.Forms.PictureBox();
            this.pctYellow1 = new System.Windows.Forms.PictureBox();
            this.pctGreen1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pctGreen3 = new System.Windows.Forms.PictureBox();
            this.pctYellow3 = new System.Windows.Forms.PictureBox();
            this.pctRed3 = new System.Windows.Forms.PictureBox();
            this.pctRed2 = new System.Windows.Forms.PictureBox();
            this.pctYellow2 = new System.Windows.Forms.PictureBox();
            this.pctGreen2 = new System.Windows.Forms.PictureBox();
            this.pctRed4 = new System.Windows.Forms.PictureBox();
            this.pctYellow4 = new System.Windows.Forms.PictureBox();
            this.pctGreen4 = new System.Windows.Forms.PictureBox();
            this.lblTime = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtLebarJalan = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.lblKepadatanJalan = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.vlcControl2 = new Vlc.DotNet.Forms.VlcControl();
            this.label17 = new System.Windows.Forms.Label();
            this.vlcControl3 = new Vlc.DotNet.Forms.VlcControl();
            this.label18 = new System.Windows.Forms.Label();
            this.vlcControl4 = new Vlc.DotNet.Forms.VlcControl();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lblPixelJalan = new System.Windows.Forms.Label();
            this.lblPixelKendaraan = new System.Windows.Forms.Label();
            this.lblSimpang = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.vlcControl1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctUji)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctCCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctMorfologi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctSubstraksi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctElimination)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctHoughSelection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctHough)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctEdge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctSegmentasi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctRoi)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctRed1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctYellow1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGreen1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGreen3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctYellow3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctRed3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctRed2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctYellow2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGreen2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctRed4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctYellow4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGreen4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlcControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlcControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlcControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // vlcControl1
            // 
            this.vlcControl1.BackColor = System.Drawing.Color.Black;
            this.vlcControl1.Location = new System.Drawing.Point(18, 78);
            this.vlcControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.vlcControl1.Name = "vlcControl1";
            this.vlcControl1.Size = new System.Drawing.Size(405, 277);
            this.vlcControl1.Spu = -1;
            this.vlcControl1.TabIndex = 0;
            this.vlcControl1.Text = "vlcControl1";
            this.vlcControl1.VlcLibDirectory = ((System.IO.DirectoryInfo)(resources.GetObject("vlcControl1.VlcLibDirectory")));
            this.vlcControl1.VlcMediaplayerOptions = null;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(1474, 86);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(112, 35);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDetail);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.pctResult);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.pctUji);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.pctCCL);
            this.groupBox1.Controls.Add(this.pctMorfologi);
            this.groupBox1.Controls.Add(this.pctGT);
            this.groupBox1.Controls.Add(this.pctSubstraksi);
            this.groupBox1.Controls.Add(this.pctElimination);
            this.groupBox1.Controls.Add(this.pctHoughSelection);
            this.groupBox1.Controls.Add(this.pctHough);
            this.groupBox1.Controls.Add(this.pctEdge);
            this.groupBox1.Controls.Add(this.pctSegmentasi);
            this.groupBox1.Controls.Add(this.pctRoi);
            this.groupBox1.Location = new System.Drawing.Point(18, 675);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1860, 400);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Image Processing";
            // 
            // txtDetail
            // 
            this.txtDetail.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDetail.Location = new System.Drawing.Point(1442, 26);
            this.txtDetail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDetail.Multiline = true;
            this.txtDetail.Name = "txtDetail";
            this.txtDetail.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtDetail.Size = new System.Drawing.Size(408, 356);
            this.txtDetail.TabIndex = 49;
            this.txtDetail.WordWrap = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1180, 212);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 20);
            this.label7.TabIndex = 48;
            this.label7.Text = "Result";
            // 
            // pctResult
            // 
            this.pctResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctResult.Location = new System.Drawing.Point(1185, 237);
            this.pctResult.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctResult.Name = "pctResult";
            this.pctResult.Size = new System.Drawing.Size(224, 153);
            this.pctResult.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctResult.TabIndex = 47;
            this.pctResult.TabStop = false;
            this.pctResult.Paint += new System.Windows.Forms.PaintEventHandler(this.pctResult_Paint);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(946, 212);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(238, 20);
            this.label11.TabIndex = 46;
            this.label11.Text = "Connected Component Labeling";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(712, 212);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(235, 20);
            this.label10.TabIndex = 45;
            this.label10.Text = "Morfologi (Opening and Closing)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(478, 212);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(185, 20);
            this.label9.TabIndex = 44;
            this.label9.Text = "Grayscale and Threshold";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(244, 212);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 20);
            this.label8.TabIndex = 43;
            this.label8.Text = "Subtraksi";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 212);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 20);
            this.label1.TabIndex = 42;
            this.label1.Text = "Citra Uji";
            // 
            // pctUji
            // 
            this.pctUji.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctUji.Location = new System.Drawing.Point(15, 237);
            this.pctUji.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctUji.Name = "pctUji";
            this.pctUji.Size = new System.Drawing.Size(224, 153);
            this.pctUji.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctUji.TabIndex = 41;
            this.pctUji.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(946, 26);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(203, 20);
            this.label5.TabIndex = 40;
            this.label5.Text = "Hough Transform Selection";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(712, 26);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(133, 20);
            this.label19.TabIndex = 39;
            this.label19.Text = "Hough Transform";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(478, 26);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 20);
            this.label4.TabIndex = 38;
            this.label4.Text = "Deteksi Tepi";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(244, 26);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 20);
            this.label3.TabIndex = 37;
            this.label3.Text = "Segmentasi";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1180, 26);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 20);
            this.label6.TabIndex = 36;
            this.label6.Text = "Eliminasi";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 26);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 20);
            this.label2.TabIndex = 23;
            this.label2.Text = "Citra ROI";
            // 
            // pctCCL
            // 
            this.pctCCL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctCCL.Location = new System.Drawing.Point(951, 237);
            this.pctCCL.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctCCL.Name = "pctCCL";
            this.pctCCL.Size = new System.Drawing.Size(224, 153);
            this.pctCCL.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctCCL.TabIndex = 9;
            this.pctCCL.TabStop = false;
            // 
            // pctMorfologi
            // 
            this.pctMorfologi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctMorfologi.Location = new System.Drawing.Point(717, 237);
            this.pctMorfologi.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctMorfologi.Name = "pctMorfologi";
            this.pctMorfologi.Size = new System.Drawing.Size(224, 153);
            this.pctMorfologi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctMorfologi.TabIndex = 8;
            this.pctMorfologi.TabStop = false;
            // 
            // pctGT
            // 
            this.pctGT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctGT.Location = new System.Drawing.Point(483, 237);
            this.pctGT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctGT.Name = "pctGT";
            this.pctGT.Size = new System.Drawing.Size(224, 153);
            this.pctGT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctGT.TabIndex = 7;
            this.pctGT.TabStop = false;
            // 
            // pctSubstraksi
            // 
            this.pctSubstraksi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctSubstraksi.Location = new System.Drawing.Point(249, 237);
            this.pctSubstraksi.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctSubstraksi.Name = "pctSubstraksi";
            this.pctSubstraksi.Size = new System.Drawing.Size(224, 153);
            this.pctSubstraksi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctSubstraksi.TabIndex = 6;
            this.pctSubstraksi.TabStop = false;
            // 
            // pctElimination
            // 
            this.pctElimination.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctElimination.Location = new System.Drawing.Point(1185, 51);
            this.pctElimination.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctElimination.Name = "pctElimination";
            this.pctElimination.Size = new System.Drawing.Size(224, 153);
            this.pctElimination.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctElimination.TabIndex = 5;
            this.pctElimination.TabStop = false;
            // 
            // pctHoughSelection
            // 
            this.pctHoughSelection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctHoughSelection.Location = new System.Drawing.Point(951, 51);
            this.pctHoughSelection.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctHoughSelection.Name = "pctHoughSelection";
            this.pctHoughSelection.Size = new System.Drawing.Size(224, 153);
            this.pctHoughSelection.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctHoughSelection.TabIndex = 4;
            this.pctHoughSelection.TabStop = false;
            // 
            // pctHough
            // 
            this.pctHough.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctHough.Location = new System.Drawing.Point(717, 51);
            this.pctHough.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctHough.Name = "pctHough";
            this.pctHough.Size = new System.Drawing.Size(224, 153);
            this.pctHough.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctHough.TabIndex = 3;
            this.pctHough.TabStop = false;
            // 
            // pctEdge
            // 
            this.pctEdge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctEdge.Location = new System.Drawing.Point(483, 51);
            this.pctEdge.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctEdge.Name = "pctEdge";
            this.pctEdge.Size = new System.Drawing.Size(224, 153);
            this.pctEdge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctEdge.TabIndex = 2;
            this.pctEdge.TabStop = false;
            // 
            // pctSegmentasi
            // 
            this.pctSegmentasi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctSegmentasi.Location = new System.Drawing.Point(249, 51);
            this.pctSegmentasi.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctSegmentasi.Name = "pctSegmentasi";
            this.pctSegmentasi.Size = new System.Drawing.Size(224, 153);
            this.pctSegmentasi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctSegmentasi.TabIndex = 1;
            this.pctSegmentasi.TabStop = false;
            // 
            // pctRoi
            // 
            this.pctRoi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctRoi.Location = new System.Drawing.Point(15, 51);
            this.pctRoi.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctRoi.Name = "pctRoi";
            this.pctRoi.Size = new System.Drawing.Size(224, 153);
            this.pctRoi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctRoi.TabIndex = 0;
            this.pctRoi.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(9, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1896, 35);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trainingToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(69, 29);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // trainingToolStripMenuItem
            // 
            this.trainingToolStripMenuItem.Name = "trainingToolStripMenuItem";
            this.trainingToolStripMenuItem.Size = new System.Drawing.Size(157, 30);
            this.trainingToolStripMenuItem.Text = "Training";
            this.trainingToolStripMenuItem.Click += new System.EventHandler(this.trainingToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(157, 30);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Location = new System.Drawing.Point(1700, 42);
            this.btnOpenFile.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(178, 35);
            this.btnOpenFile.TabIndex = 4;
            this.btnOpenFile.Text = "Open Traning Data";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // txtTraningFile
            // 
            this.txtTraningFile.Location = new System.Drawing.Point(945, 46);
            this.txtTraningFile.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTraningFile.Name = "txtTraningFile";
            this.txtTraningFile.Size = new System.Drawing.Size(712, 26);
            this.txtTraningFile.TabIndex = 5;
            // 
            // pctRed1
            // 
            this.pctRed1.Image = global::SimulasiPengaturanLaluLintasDinamis.Properties.Resources.red;
            this.pctRed1.Location = new System.Drawing.Point(1030, 400);
            this.pctRed1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctRed1.Name = "pctRed1";
            this.pctRed1.Size = new System.Drawing.Size(30, 31);
            this.pctRed1.TabIndex = 10;
            this.pctRed1.TabStop = false;
            // 
            // pctYellow1
            // 
            this.pctYellow1.Image = global::SimulasiPengaturanLaluLintasDinamis.Properties.Resources.black;
            this.pctYellow1.Location = new System.Drawing.Point(1070, 400);
            this.pctYellow1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctYellow1.Name = "pctYellow1";
            this.pctYellow1.Size = new System.Drawing.Size(30, 31);
            this.pctYellow1.TabIndex = 9;
            this.pctYellow1.TabStop = false;
            // 
            // pctGreen1
            // 
            this.pctGreen1.Image = global::SimulasiPengaturanLaluLintasDinamis.Properties.Resources.black;
            this.pctGreen1.Location = new System.Drawing.Point(1108, 400);
            this.pctGreen1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctGreen1.Name = "pctGreen1";
            this.pctGreen1.Size = new System.Drawing.Size(30, 31);
            this.pctGreen1.TabIndex = 8;
            this.pctGreen1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox1.Image = global::SimulasiPengaturanLaluLintasDinamis.Properties.Resources.street;
            this.pictureBox1.Location = new System.Drawing.Point(945, 86);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(501, 514);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // pctGreen3
            // 
            this.pctGreen3.Image = global::SimulasiPengaturanLaluLintasDinamis.Properties.Resources.black;
            this.pctGreen3.Location = new System.Drawing.Point(1250, 255);
            this.pctGreen3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctGreen3.Name = "pctGreen3";
            this.pctGreen3.Size = new System.Drawing.Size(30, 31);
            this.pctGreen3.TabIndex = 13;
            this.pctGreen3.TabStop = false;
            // 
            // pctYellow3
            // 
            this.pctYellow3.Image = global::SimulasiPengaturanLaluLintasDinamis.Properties.Resources.black;
            this.pctYellow3.Location = new System.Drawing.Point(1288, 255);
            this.pctYellow3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctYellow3.Name = "pctYellow3";
            this.pctYellow3.Size = new System.Drawing.Size(30, 31);
            this.pctYellow3.TabIndex = 12;
            this.pctYellow3.TabStop = false;
            // 
            // pctRed3
            // 
            this.pctRed3.Image = global::SimulasiPengaturanLaluLintasDinamis.Properties.Resources.red;
            this.pctRed3.Location = new System.Drawing.Point(1328, 255);
            this.pctRed3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctRed3.Name = "pctRed3";
            this.pctRed3.Size = new System.Drawing.Size(30, 31);
            this.pctRed3.TabIndex = 11;
            this.pctRed3.TabStop = false;
            // 
            // pctRed2
            // 
            this.pctRed2.Image = global::SimulasiPengaturanLaluLintasDinamis.Properties.Resources.red;
            this.pctRed2.Location = new System.Drawing.Point(1250, 480);
            this.pctRed2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctRed2.Name = "pctRed2";
            this.pctRed2.Size = new System.Drawing.Size(30, 31);
            this.pctRed2.TabIndex = 14;
            this.pctRed2.TabStop = false;
            // 
            // pctYellow2
            // 
            this.pctYellow2.Image = global::SimulasiPengaturanLaluLintasDinamis.Properties.Resources.black;
            this.pctYellow2.Location = new System.Drawing.Point(1250, 440);
            this.pctYellow2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctYellow2.Name = "pctYellow2";
            this.pctYellow2.Size = new System.Drawing.Size(30, 31);
            this.pctYellow2.TabIndex = 15;
            this.pctYellow2.TabStop = false;
            // 
            // pctGreen2
            // 
            this.pctGreen2.Image = global::SimulasiPengaturanLaluLintasDinamis.Properties.Resources.black;
            this.pctGreen2.Location = new System.Drawing.Point(1250, 400);
            this.pctGreen2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctGreen2.Name = "pctGreen2";
            this.pctGreen2.Size = new System.Drawing.Size(30, 31);
            this.pctGreen2.TabIndex = 16;
            this.pctGreen2.TabStop = false;
            // 
            // pctRed4
            // 
            this.pctRed4.Image = global::SimulasiPengaturanLaluLintasDinamis.Properties.Resources.red;
            this.pctRed4.Location = new System.Drawing.Point(1108, 175);
            this.pctRed4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctRed4.Name = "pctRed4";
            this.pctRed4.Size = new System.Drawing.Size(30, 31);
            this.pctRed4.TabIndex = 19;
            this.pctRed4.TabStop = false;
            // 
            // pctYellow4
            // 
            this.pctYellow4.Image = global::SimulasiPengaturanLaluLintasDinamis.Properties.Resources.black;
            this.pctYellow4.Location = new System.Drawing.Point(1108, 215);
            this.pctYellow4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctYellow4.Name = "pctYellow4";
            this.pctYellow4.Size = new System.Drawing.Size(30, 31);
            this.pctYellow4.TabIndex = 18;
            this.pctYellow4.TabStop = false;
            // 
            // pctGreen4
            // 
            this.pctGreen4.Image = global::SimulasiPengaturanLaluLintasDinamis.Properties.Resources.black;
            this.pctGreen4.Location = new System.Drawing.Point(1108, 255);
            this.pctGreen4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctGreen4.Name = "pctGreen4";
            this.pctGreen4.Size = new System.Drawing.Size(30, 31);
            this.pctGreen4.TabIndex = 17;
            this.pctGreen4.TabStop = false;
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.Location = new System.Drawing.Point(1474, 143);
            this.lblTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(76, 82);
            this.lblTime.TabIndex = 20;
            this.lblTime.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(1491, 266);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 20);
            this.label12.TabIndex = 21;
            this.label12.Text = "Lebar Jalan";
            // 
            // txtLebarJalan
            // 
            this.txtLebarJalan.Location = new System.Drawing.Point(1679, 262);
            this.txtLebarJalan.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtLebarJalan.Name = "txtLebarJalan";
            this.txtLebarJalan.Size = new System.Drawing.Size(76, 26);
            this.txtLebarJalan.TabIndex = 22;
            this.txtLebarJalan.Text = "4";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1491, 311);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(129, 20);
            this.label13.TabIndex = 23;
            this.label13.Text = "Kepadatan Jalan";
            // 
            // lblKepadatanJalan
            // 
            this.lblKepadatanJalan.AutoSize = true;
            this.lblKepadatanJalan.Location = new System.Drawing.Point(1675, 311);
            this.lblKepadatanJalan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKepadatanJalan.Name = "lblKepadatanJalan";
            this.lblKepadatanJalan.Size = new System.Drawing.Size(36, 20);
            this.lblKepadatanJalan.TabIndex = 25;
            this.lblKepadatanJalan.Text = "0 %";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(1768, 265);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 20);
            this.label14.TabIndex = 26;
            this.label14.Text = "m";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(14, 51);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 20);
            this.label15.TabIndex = 27;
            this.label15.Text = "Simpang 1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(483, 51);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 20);
            this.label16.TabIndex = 29;
            this.label16.Text = "Simpang 2";
            // 
            // vlcControl2
            // 
            this.vlcControl2.BackColor = System.Drawing.Color.Black;
            this.vlcControl2.Location = new System.Drawing.Point(488, 78);
            this.vlcControl2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.vlcControl2.Name = "vlcControl2";
            this.vlcControl2.Size = new System.Drawing.Size(405, 277);
            this.vlcControl2.Spu = -1;
            this.vlcControl2.TabIndex = 28;
            this.vlcControl2.Text = "vlcControl1";
            this.vlcControl2.VlcLibDirectory = ((System.IO.DirectoryInfo)(resources.GetObject("vlcControl2.VlcLibDirectory")));
            this.vlcControl2.VlcMediaplayerOptions = null;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(14, 362);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 20);
            this.label17.TabIndex = 31;
            this.label17.Text = "Simpang 3";
            // 
            // vlcControl3
            // 
            this.vlcControl3.BackColor = System.Drawing.Color.Black;
            this.vlcControl3.Location = new System.Drawing.Point(18, 389);
            this.vlcControl3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.vlcControl3.Name = "vlcControl3";
            this.vlcControl3.Size = new System.Drawing.Size(405, 277);
            this.vlcControl3.Spu = -1;
            this.vlcControl3.TabIndex = 30;
            this.vlcControl3.Text = "vlcControl1";
            this.vlcControl3.VlcLibDirectory = ((System.IO.DirectoryInfo)(resources.GetObject("vlcControl3.VlcLibDirectory")));
            this.vlcControl3.VlcMediaplayerOptions = null;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(483, 362);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(85, 20);
            this.label18.TabIndex = 33;
            this.label18.Text = "Simpang 4";
            // 
            // vlcControl4
            // 
            this.vlcControl4.BackColor = System.Drawing.Color.Black;
            this.vlcControl4.Location = new System.Drawing.Point(488, 389);
            this.vlcControl4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.vlcControl4.Name = "vlcControl4";
            this.vlcControl4.Size = new System.Drawing.Size(405, 277);
            this.vlcControl4.Spu = -1;
            this.vlcControl4.TabIndex = 32;
            this.vlcControl4.Text = "vlcControl1";
            this.vlcControl4.VlcLibDirectory = ((System.IO.DirectoryInfo)(resources.GetObject("vlcControl4.VlcLibDirectory")));
            this.vlcControl4.VlcMediaplayerOptions = null;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(1491, 353);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(122, 20);
            this.label20.TabIndex = 34;
            this.label20.Text = "Total Pixel Jalan";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(1491, 393);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(162, 20);
            this.label21.TabIndex = 35;
            this.label21.Text = "Total Pixel Kendaraan";
            // 
            // lblPixelJalan
            // 
            this.lblPixelJalan.AutoSize = true;
            this.lblPixelJalan.Location = new System.Drawing.Point(1675, 353);
            this.lblPixelJalan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPixelJalan.Name = "lblPixelJalan";
            this.lblPixelJalan.Size = new System.Drawing.Size(18, 20);
            this.lblPixelJalan.TabIndex = 36;
            this.lblPixelJalan.Text = "0";
            // 
            // lblPixelKendaraan
            // 
            this.lblPixelKendaraan.AutoSize = true;
            this.lblPixelKendaraan.Location = new System.Drawing.Point(1675, 389);
            this.lblPixelKendaraan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPixelKendaraan.Name = "lblPixelKendaraan";
            this.lblPixelKendaraan.Size = new System.Drawing.Size(18, 20);
            this.lblPixelKendaraan.TabIndex = 37;
            this.lblPixelKendaraan.Text = "0";
            // 
            // lblSimpang
            // 
            this.lblSimpang.AutoSize = true;
            this.lblSimpang.Location = new System.Drawing.Point(1675, 430);
            this.lblSimpang.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSimpang.Name = "lblSimpang";
            this.lblSimpang.Size = new System.Drawing.Size(18, 20);
            this.lblSimpang.TabIndex = 39;
            this.lblSimpang.Text = "1";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(1491, 430);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(108, 20);
            this.label22.TabIndex = 38;
            this.label22.Text = "Simpang Aktif";
            // 
            // FormUtama
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1896, 1080);
            this.Controls.Add(this.lblSimpang);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.lblPixelKendaraan);
            this.Controls.Add(this.lblPixelJalan);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.vlcControl4);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.vlcControl3);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.vlcControl2);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lblKepadatanJalan);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtLebarJalan);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.pctRed4);
            this.Controls.Add(this.pctYellow4);
            this.Controls.Add(this.pctGreen4);
            this.Controls.Add(this.pctGreen2);
            this.Controls.Add(this.pctYellow2);
            this.Controls.Add(this.pctRed2);
            this.Controls.Add(this.pctGreen3);
            this.Controls.Add(this.pctYellow3);
            this.Controls.Add(this.pctRed3);
            this.Controls.Add(this.pctRed1);
            this.Controls.Add(this.pctYellow1);
            this.Controls.Add(this.pctGreen1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtTraningFile);
            this.Controls.Add(this.btnOpenFile);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.vlcControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormUtama";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simulasi Pengaturan Lalu Lintas Dinamis";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormUtama_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.vlcControl1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctUji)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctCCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctMorfologi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctSubstraksi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctElimination)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctHoughSelection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctHough)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctEdge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctSegmentasi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctRoi)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctRed1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctYellow1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGreen1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGreen3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctYellow3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctRed3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctRed2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctYellow2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGreen2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctRed4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctYellow4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGreen4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlcControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlcControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlcControl4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Vlc.DotNet.Forms.VlcControl vlcControl1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pctRoi;
        private System.Windows.Forms.PictureBox pctCCL;
        private System.Windows.Forms.PictureBox pctMorfologi;
        private System.Windows.Forms.PictureBox pctGT;
        private System.Windows.Forms.PictureBox pctSubstraksi;
        private System.Windows.Forms.PictureBox pctElimination;
        private System.Windows.Forms.PictureBox pctHoughSelection;
        private System.Windows.Forms.PictureBox pctHough;
        private System.Windows.Forms.PictureBox pctEdge;
        private System.Windows.Forms.PictureBox pctSegmentasi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pctUji;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pctResult;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trainingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtTraningFile;
        private System.Windows.Forms.TextBox txtDetail;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pctGreen1;
        private System.Windows.Forms.PictureBox pctYellow1;
        private System.Windows.Forms.PictureBox pctRed1;
        private System.Windows.Forms.PictureBox pctGreen3;
        private System.Windows.Forms.PictureBox pctYellow3;
        private System.Windows.Forms.PictureBox pctRed3;
        private System.Windows.Forms.PictureBox pctRed2;
        private System.Windows.Forms.PictureBox pctYellow2;
        private System.Windows.Forms.PictureBox pctGreen2;
        private System.Windows.Forms.PictureBox pctRed4;
        private System.Windows.Forms.PictureBox pctYellow4;
        private System.Windows.Forms.PictureBox pctGreen4;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtLebarJalan;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblKepadatanJalan;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private Vlc.DotNet.Forms.VlcControl vlcControl2;
        private System.Windows.Forms.Label label17;
        private Vlc.DotNet.Forms.VlcControl vlcControl3;
        private System.Windows.Forms.Label label18;
        private Vlc.DotNet.Forms.VlcControl vlcControl4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblPixelJalan;
        private System.Windows.Forms.Label lblPixelKendaraan;
        private System.Windows.Forms.Label lblSimpang;
        private System.Windows.Forms.Label label22;
    }
}

