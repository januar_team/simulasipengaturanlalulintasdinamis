﻿using SimulasiPengaturanLaluLintasDinamis.Neuro.Membership;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulasiPengaturanLaluLintasDinamis.Neuro
{
    class TraningData
    {
        public int NumberOfAtribute { set; get; }

        public int NumberOfCluster { get; set; }

        public GenerelizedBell[][] MembershipFunction { get; set; }

        public double[,] forthLayer;

        public TraningData()
        {

        }

        public TraningData(ANFIS anfis)
        {
            NumberOfAtribute = anfis.NumberOfAtribute;
            NumberOfCluster = anfis.NumberOfCluster;
            MembershipFunction = anfis.MembershipFunction;
            forthLayer = anfis.getForthLayer();
        }
    }
}
