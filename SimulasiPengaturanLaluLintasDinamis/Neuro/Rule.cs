﻿using SimulasiPengaturanLaluLintasDinamis.Neuro.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulasiPengaturanLaluLintasDinamis.Neuro
{
    class Rule : IRule
    {
        private List<IMembershipFunction> functions;

        public double[] ActivationValue { get; set; }

        public Rule(List<IMembershipFunction> func) {
            functions = func;
        }

        public double Calculate(double[] input)
        {
            double result = 0.0d;
            ActivationValue = new double[input.Length];

            for (int i = 0; i < input.Length; i++) {
                result += ActivationValue[i] = functions[i].Activate(input[i]);
            }

            return result;
        }
    }
}
