﻿using SimulasiPengaturanLaluLintasDinamis.Neuro.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulasiPengaturanLaluLintasDinamis.Neuro.Membership
{
    class GenerelizedBell : IMembershipFunction
    {
        public double a, b, c;

        public GenerelizedBell(double a, double b, double c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public double Activate(double x)
        {
            return 1 / (1 + Math.Pow((x - c)/a, 2 * b));
        }
    }
}
