﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulasiPengaturanLaluLintasDinamis.Neuro
{
    class Helper
    {
        private static Random rnd;

        public static double Random(int dec) {
            if (rnd == null)
                rnd = new System.Random();

            return Math.Round(rnd.NextDouble(), dec);
        }

        public static double Random() {
            return Random(1);
        }

        public static double mean(double[] data) {
            return data.Sum() / data.Length;
        }

        public static double deviation(double[] data)
        {
            double sum_x = 0.0d;
            double sum_x_square = 0.0d;
            for (int i = 0; i < data.Length; i++)
            {
                sum_x += data[i];
                sum_x_square += Math.Pow(data[i], 2);
            }

            return ((data.Length * sum_x_square - Math.Pow(sum_x, 2)) / (data.Length * (data.Length - 1)));
        }
    }
}
