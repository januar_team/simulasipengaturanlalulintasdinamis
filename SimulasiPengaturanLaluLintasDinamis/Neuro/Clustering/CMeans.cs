﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulasiPengaturanLaluLintasDinamis.Neuro.Clustering
{
    class CMeans
    {
        private int total_cluster;

        public int TotalCluster {
            get
            {
                return total_cluster;
            }
            set
            {
                this.total_cluster = value;
            }
        }

        public int Weight { get; set; }
        public int MaxIteration { get; set; }
        public double Error { get; set; }
        public double ObjectiveFuction { get; set; }
        public int TotalAtribute { get; set; }

        public List<DataItem> Data;
        public double[][] MembershipGrade;

        public CMeans(int total_cluster, int weight, double error, int max_iteration)
        {
            TotalCluster = total_cluster;
            Weight = weight;
            Error = error;
            MaxIteration = max_iteration;
            ObjectiveFuction = 0.0d;
        }

        public void GenerateMembershipGrade() {
            if (Data == null)
                throw new DataNotFoundException();

            MembershipGrade = new double[Data.Count][];
            for (int i = 0; i < MembershipGrade.Length; i++) {
                MembershipGrade[i] = new double[TotalCluster];
                for (int j = 0; j < TotalCluster; j++)
                {
                    MembershipGrade[i][j] = Helper.Random();
                }
            }
        }

        public double[][] CalculateClusterCenter() {
            double[][] centroid = new double[TotalCluster][];

            Console.WriteLine("Centroid");
            for (int i = 0; i < TotalCluster; i++)
            {
                double[] temp = new double[TotalAtribute];
                double sigma_u = 0.0d;
                for (int j = 0; j < Data.Count; j++)
                {
                    double u = Math.Pow(MembershipGrade[j][i], Weight);
                    sigma_u += u;
                    for (int k = 0; k < TotalAtribute; k++)
                    {
                        temp[k] += Data[j].Data[k] * u;
                    }
                }

                centroid[i] = new double[TotalAtribute];
                for(int j = 0; j < TotalAtribute; j++)
                {
                    centroid[i][j] = Math.Round(temp[j] / sigma_u, 6);
                    //centroid[i][j] = temp[j] / sigma_u;
                    Console.Write("{0} | ", centroid[i][j]);
                }
                Console.WriteLine();
            }
            return centroid;
        }

        public double CalculateObjectiveFunction(double[][] centroid)
        {
            double result = 0.0d;

            for (int i = 0; i < Data.Count; i++)
            {
                double[] tempC = new double[TotalCluster];
                for (int j = 0; j < TotalCluster; j++)
                {
                    double[] tempA = new double[TotalAtribute];
                    for(int k = 0; k < TotalAtribute; k++)
                    {
                        tempA[k] += Math.Round(Math.Pow(Data[i].Data[k] - centroid[j][k], 2), 6);
                        //tempA[k] += Math.Pow(Data[i].Data[k] - centroid[j][k], 2);
                    }

                    tempC[j] = Math.Round(tempA.Sum() * Math.Pow(MembershipGrade[i][j], Weight), 6);
                    //tempC[j] = tempA.Sum() * Math.Pow(MembershipGrade[i][j], Weight);
                }
                result += tempC.Sum();
            }
            Console.WriteLine("Objective Function : {0}", result);
            return result;
        }

        public void RegenerateMembershipGrade(double[][] centroid) {
            for (int i = 0; i < MembershipGrade.Length; i++)
            {
                double[] tempL = new double[TotalCluster];
                for (int j = 0; j < TotalCluster; j++)
                {
                    double[] tempA = new double[TotalAtribute];
                    for (int k = 0; k < TotalAtribute; k++)
                    {
                        tempA[k] += Math.Round(Math.Pow(Data[i].Data[k] - centroid[j][k], 2), 6);
                        //tempA[k] += Math.Pow(Data[i].Data[k] - centroid[j][k], 2);
                    }

                    tempL[j] = Math.Round(tempA.Sum() * Math.Pow(MembershipGrade[i][j], Weight), 6);
                    //tempL[j] = tempA.Sum() * Math.Pow(MembershipGrade[i][j], Weight);
                }

                for(int j = 0; j < MembershipGrade[i].Length; j++)
                {
                    MembershipGrade[i][j] = Math.Round(tempL[j] / tempL.Sum(), 6);
                    //MembershipGrade[i][j] = tempL[j] / tempL.Sum();
                }
            }
        }

        public double[][] Training()
        {
            GenerateMembershipGrade();
            int i = 0;
            double obj = 0.0d;
            double[][] centroid;
            do
            {
                Console.WriteLine("Iteration : {0}", i+1);
                ObjectiveFuction = obj;
                centroid = CalculateClusterCenter();
                obj = CalculateObjectiveFunction(centroid);
                RegenerateMembershipGrade(centroid);
                i++;
                Console.WriteLine("===================================================\n");
            } while ((i < MaxIteration) && (Math.Abs(obj - ObjectiveFuction) > Error));

            for (i = 0; i < Data.Count; i++) {
                int cluster = 0;
                Data[i].JarakCluster = new double[TotalCluster];
                for (int j = 0; j < centroid.Length; j++) {
                    Data[i].JarakCluster[j] = Data[i].EuclideanDistance(centroid[j]);

                    if (j == 0)
                        continue;
                    else
                    {

                        if (Data[i].JarakCluster[j] < Data[i].JarakCluster[cluster])
                        {
                            cluster = j;
                        }
                    }
                }
                Data[i].Cluster = cluster;
            }
            return centroid;
        }
    }
}
