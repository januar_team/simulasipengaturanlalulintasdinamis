﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulasiPengaturanLaluLintasDinamis.Neuro.Clustering
{
    class DataNotFoundException : Exception
    {
        public DataNotFoundException() : base("Data not initialize") { }
    }
}
