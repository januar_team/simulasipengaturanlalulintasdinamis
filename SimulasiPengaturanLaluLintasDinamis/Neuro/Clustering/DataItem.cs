﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulasiPengaturanLaluLintasDinamis.Neuro.Clustering
{
    class DataItem
    {
        private double[] data;
        private double[] jarak_cluster;
        private int cluster;
        
        public double[] Data
        {
            get { return data; }
            set { data = value; }
        }

        public double[] JarakCluster
        {
            get { return jarak_cluster; }
            set { jarak_cluster = value; }
        }

        public int Cluster
        {
            get { return cluster; }
            set { cluster = value; }
        }

        public DataItem() { }

        public double EuclideanDistance(double[] cluster) {
            double distance = 0.0d;
            for (int i = 0; i < Data.Length; i++)
            {
                distance += Math.Pow(cluster[i] - this.Data[i], 2);
            }
            distance = Math.Sqrt(distance);

            return distance;
        }


    }
}
