﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulasiPengaturanLaluLintasDinamis.Neuro.Interface
{
    interface IRule
    {
        double Calculate(double[] input);
    }
}
