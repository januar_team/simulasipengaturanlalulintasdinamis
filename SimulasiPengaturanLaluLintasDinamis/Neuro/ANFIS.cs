﻿using SimulasiPengaturanLaluLintasDinamis.Neuro.Clustering;
using SimulasiPengaturanLaluLintasDinamis.Neuro.Interface;
using SimulasiPengaturanLaluLintasDinamis.Neuro.Membership;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulasiPengaturanLaluLintasDinamis.Neuro
{
    class ANFIS
    {
        private double[][] secondLayer;

        private double[][] thirdLayer;

        private double[,] forthLayer;

        private double[,] forthLayerTemp;

        public double[] Result;

        public int NumberOfAtribute { set; get; }

        public int NumberOfCluster { get; set; }

        public List<DataItem> DataTraning { set; get; }

        public double[,] DataOutput { set; get; }

        public GenerelizedBell[][] MembershipFunction { get; set; }
        public double[][] MembershipGrade { get; set; }

        public List<IRule> Rules { get; set; }

        public StringBuilder sb { set; get; }

        public ANFIS(List<DataItem> traning, double[,] output)
        {
            DataTraning = traning;
            DataOutput = output;
            secondLayer = new double[DataTraning.Count][];
            thirdLayer = new double[DataTraning.Count][];
        }

        public ANFIS(TraningData traning)
        {
            NumberOfAtribute = traning.NumberOfAtribute;
            NumberOfCluster = traning.NumberOfCluster;
            MembershipFunction = traning.MembershipFunction;
            forthLayer = traning.forthLayer;
        }

        public double[,] getForthLayer()
        {
            return this.forthLayer;
        }

        public void Traning()
        {
            GenerateMembershipFunction();
            CalculateMembershipGrade();

            sb.Append(String.Format("\r\nFirst Layer\r\n"));
            General.TotalColumn = 4;
            sb.Append(General.DrawLine());
            for (int i = 0; i < MembershipGrade.Length; i++)
            {
                string[] temp = new string[MembershipGrade[i].Length];
                for(int j = 0; j < MembershipGrade[i].Length; j++)
                {
                    temp[j] = MembershipGrade[i][j].ToString();
                }
                sb.Append(General.DrawRow(temp));
            }
            sb.Append(General.DrawLine());

            SecondLayer();
            General.TotalColumn = 2;
            sb.Append(String.Format("\r\nSecond Layer\r\n"));
            sb.Append(General.DrawLine());
            for (int i = 0; i < secondLayer.Length; i++)
            {
                string[] temp = new string[secondLayer[i].Length];
                for (int j = 0; j < secondLayer[i].Length; j++)
                {
                    temp[j] = secondLayer[i][j].ToString();
                }
                sb.Append(General.DrawRow(temp));
            }
            sb.Append(General.DrawLine());

            ThirdLayer();
            sb.Append(String.Format("\r\nThird Layer\r\n"));
            sb.Append(General.DrawLine());
            for (int i = 0; i < thirdLayer.Length; i++)
            {
                string[] temp = new string[thirdLayer[i].Length];
                for (int j = 0; j < thirdLayer[i].Length; j++)
                {
                    temp[j] = thirdLayer[i][j].ToString();
                }
                sb.Append(General.DrawRow(temp));
            }
            sb.Append(General.DrawLine());

            ForthLayer(true);
            General.TotalColumn = 1;
            sb.Append("\r\n");
            sb.Append(General.DrawLine());
            for (int i = 0; i < forthLayer.Length; i++)
            {
                sb.Append(General.DrawRow(new string[] { forthLayer[i, 0].ToString() }));
            }
            sb.Append(General.DrawLine());
            Result = FifthLayer();

            sb.Append("\r\nFifth Layer - Result \r\n");
            sb.Append(General.DrawLine());
            for (int i = 0; i < Result.Length; i++)
            {
                sb.Append(General.DrawRow(new string[] { Result[i].ToString() }));
            }
            sb.Append(General.DrawLine());
        }

        public double[] Execute(List<DataItem> data)
        {
            DataTraning = data;
            secondLayer = new double[DataTraning.Count][];
            thirdLayer = new double[DataTraning.Count][];

            CalculateMembershipGrade();

            sb.Append(String.Format("\r\nFirst Layer\r\n"));
            General.TotalColumn = 4;
            sb.Append(General.DrawLine());
            for (int i = 0; i < MembershipGrade.Length; i++)
            {
                string[] temp = new string[MembershipGrade[i].Length];
                for (int j = 0; j < MembershipGrade[i].Length; j++)
                {
                    temp[j] = MembershipGrade[i][j].ToString();
                }
                sb.Append(General.DrawRow(temp));
            }
            sb.Append(General.DrawLine());

            SecondLayer();
            General.TotalColumn = 2;
            sb.Append(String.Format("\r\nSecond Layer\r\n"));
            sb.Append(General.DrawLine());
            for (int i = 0; i < secondLayer.Length; i++)
            {
                string[] temp = new string[secondLayer[i].Length];
                for (int j = 0; j < secondLayer[i].Length; j++)
                {
                    temp[j] = secondLayer[i][j].ToString();
                }
                sb.Append(General.DrawRow(temp));
            }
            sb.Append(General.DrawLine());

            ThirdLayer();
            sb.Append(String.Format("\r\nThird Layer\r\n"));
            sb.Append(General.DrawLine());
            for (int i = 0; i < thirdLayer.Length; i++)
            {
                string[] temp = new string[thirdLayer[i].Length];
                for (int j = 0; j < thirdLayer[i].Length; j++)
                {
                    temp[j] = thirdLayer[i][j].ToString();
                }
                sb.Append(General.DrawRow(temp));
            }
            sb.Append(General.DrawLine());

            ForthLayer(false);
            General.TotalColumn = 1;
            sb.Append("\r\n");
            sb.Append(General.DrawLine());
            for (int i = 0; i < forthLayer.Length; i++)
            {
                sb.Append(General.DrawRow(new string[] { forthLayer[i, 0].ToString() }));
            }
            sb.Append(General.DrawLine());
            Result = FifthLayer();

            sb.Append("\r\nFifth Layer - Result \r\n");
            sb.Append(General.DrawLine());
            for (int i = 0; i < Result.Length; i++)
            {
                sb.Append(General.DrawRow(new string[] { Result[i].ToString() }));
            }
            sb.Append(General.DrawLine());

            return Result;
        }

        private void CalculateMembershipGrade() {
            MembershipGrade = new double[DataTraning.Count][];
            for(int i = 0; i < DataTraning.Count; i++)
            {
                MembershipGrade[i] = new double[NumberOfAtribute * NumberOfCluster];
                for(int j = 0; j < MembershipFunction.Length; j++)
                {
                    for(int k = 0; k < MembershipFunction[j].Length; k++)
                    {
                        MembershipGrade[i][k * MembershipFunction[j].Length + j] = MembershipFunction[j][k].Activate(DataTraning[i].Data[j]);
                    }
                }
            }
        }

        private void GenerateMembershipFunction() {
            MembershipFunction = new GenerelizedBell[NumberOfAtribute][];
            for (int i = 0; i < NumberOfAtribute; i++)
            {
                MembershipFunction[i] = new GenerelizedBell[NumberOfCluster];
                for(int j = 0; j < NumberOfCluster; j++)
                {
                    double[] data = DataTraning.Where(x => x.Cluster == j).Select(y => y.Data[i]).ToArray();
                    double mean = Helper.mean(data);
                    double deviation = Helper.deviation(data);
                    MembershipFunction[i][j] = new GenerelizedBell(deviation, 1, mean);
                }
            }
        }

        private void SecondLayer()
        {
            for(int i = 0; i < DataTraning.Count; i++)
            {
                secondLayer[i] = new double[NumberOfCluster];
                for(int j =0; j < NumberOfCluster; j++)
                {
                    secondLayer[i][j] = 1;
                    for(int k = 0; k < NumberOfAtribute; k++)
                    {
                        secondLayer[i][j] *= MembershipGrade[i][k * NumberOfAtribute + j];
                    }
                }
            }
        }

        private void ThirdLayer()
        {
            for(int i =0; i < DataTraning.Count; i++)
            {
                thirdLayer[i] = new double[NumberOfCluster];
                for(int j = 0; j < NumberOfCluster; j++)
                {
                    thirdLayer[i][j] = secondLayer[i][j] / secondLayer[i].Sum();
                }
            }
        }

        private void ForthLayer(bool isTraning)
        {
            forthLayerTemp = new double[DataTraning.Count, NumberOfCluster * (NumberOfAtribute + 1)];
            for(int i = 0; i < DataTraning.Count; i++)
            {
                for(int j = 0; j < NumberOfCluster; j++)
                {
                    for(int k = 0; k < NumberOfAtribute; k++)
                    {
                        forthLayerTemp[i, j * (NumberOfCluster + 1) + k] = thirdLayer[i][j] * DataTraning[i].Data[k];
                    }
                    forthLayerTemp[i, j * (NumberOfCluster + 1) + NumberOfAtribute] = thirdLayer[i][j];
                }
            }

            sb.Append(String.Format("\r\nForth Layer\r\n"));
            if (isTraning)
            {
                General.TotalColumn = 6;
                sb.Append(General.DrawLine());
                for (int i = 0; i < DataTraning.Count; i++)
                {
                    string[] temp = new string[(NumberOfCluster * (NumberOfAtribute + 1))];
                    for (int j = 0; j < (NumberOfCluster * (NumberOfAtribute + 1)); j++)
                    {
                        temp[j] = forthLayerTemp[i, j].ToString();
                    }
                    sb.Append(General.DrawRow(temp));
                }
                sb.Append(General.DrawLine());

                Matrix A = DenseMatrix.OfArray(forthLayerTemp);
                Matrix y = DenseMatrix.OfArray(DataOutput);
                Matrix<double> result = (A.Transpose() * A).Inverse() * A.Transpose() * y;

                forthLayer = result.ToArray();
            }
        }

        private double[] FifthLayer() {
            double[] output = new double[DataTraning.Count];

            for (int i = 0; i < DataTraning.Count; i++)
            {
                for(int j = 0; j < forthLayer.Length; j++)
                {
                    output[i] += forthLayerTemp[i, j] * forthLayer[j, 0];
                }
            }
            return output;
        }
    }
}
