﻿using Newtonsoft.Json;
using SimulasiPengaturanLaluLintasDinamis.Neuro;
using SimulasiPengaturanLaluLintasDinamis.Neuro.Clustering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimulasiPengaturanLaluLintasDinamis
{
    public partial class FormTraning : Form
    {
        string traningFile;
        double[,] dataOutput;
        List<DataItem> dataTraning;

        private CMeans cmeans;
        private ANFIS anfis;
        
        public FormTraning()
        {
            InitializeComponent();
            dataTraning = new List<DataItem>();
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog.ShowDialog();
            if(result == DialogResult.OK)
            {
                StringBuilder sb = new StringBuilder();
                string path = Path.GetDirectoryName(traningFile);
                dataTraning = new List<DataItem>();
                List<double> tempOut = new List<double>();
                sb.Append("Data Traning \r\n");

                General.TotalColumn = 3;
                sb.Append(General.DrawLine());
                sb.Append(General.DrawRow(new string[] {"X1", "X2", "Target" }));
                sb.Append(General.DrawLine());
                using (StreamReader sr = new StreamReader(traningFile))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] file = sr.ReadLine().Split(new char[1] { ',' });
                        if (file.Length == 3)
                        {
                            dataTraning.Add(new DataItem() { Data = new double[] { Double.Parse(file[0]), Double.Parse(file[1]) } });
                            tempOut.Add(Double.Parse(file[2]));
                            sb.Append(General.DrawRow(file));
                        }
                    }
                }
                sb.Append(General.DrawLine());

                dataOutput = new double[tempOut.Count, 1];
                for(int i =0; i < tempOut.Count; i++)
                {
                    dataOutput[i, 0] = tempOut[i];
                }

                txtResult.Text = sb.ToString();
            }
        }

        private void openFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            traningFile = openFileDialog.FileName;
            txtFilename.Text = openFileDialog.FileName;
        }

        private void btnTraning_Click(object sender, EventArgs e)
        {
            if (dataTraning.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                cmeans = new CMeans(2, 2, 0.000001, 100);
                cmeans.TotalAtribute = 2;
                cmeans.Data = dataTraning;
                sb.Append("\r\nCentroid\r\n");
                var centroid = cmeans.Training();


                General.ColumnWidth = 20;
                General.TotalColumn = 3;
                sb.Append(General.DrawLine());
                sb.Append(General.DrawRow(new string[] { "μi1", "μi2", "Cluster" }));
                sb.Append(General.DrawLine());
                for (int i = 0; i < cmeans.Data.Count; i++)
                {
                    string[] temp = new string[cmeans.MembershipGrade[i].Length + 1];
                    for (int j = 0; j < cmeans.MembershipGrade[i].Length; j++)
                    {
                        temp[j] = cmeans.MembershipGrade[i][j].ToString();
                    }
                    temp[cmeans.MembershipGrade[i].Length] = dataTraning[i].Cluster.ToString();
                    sb.Append(General.DrawRow(temp));
                }
                sb.Append(General.DrawLine());

                anfis = new ANFIS(cmeans.Data, dataOutput);
                anfis.sb = sb;
                anfis.NumberOfAtribute = 2;
                anfis.NumberOfCluster = 2;
                anfis.Traning();

                General.TotalColumn = 1;
                General.ColumnWidth = 120;
                sb.Append(General.DrawLine());
                sb.Append(General.DrawRow(new string[] { "END" }));
                sb.Append(General.DrawLine());
                txtResult.Text += anfis.sb.ToString();
            }
            else {
                MessageBox.Show("Please select traning data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmeans != null && anfis != null)
            {
                Properties.Settings.Default.Reload();
                folderBrowserDialog.SelectedPath = Properties.Settings.Default.LastSelectedFolder;
                DialogResult result = folderBrowserDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    String path = folderBrowserDialog.SelectedPath;
                    Properties.Settings.Default.LastSelectedFolder = folderBrowserDialog.SelectedPath;
                    Properties.Settings.Default.Save();

                    String traning_data = JsonConvert.SerializeObject(new TraningData(anfis));
                    WriteToFile(path + "\\traning_app.json", traning_data);
                    General.Success("Data traning saved!");
                }
            }
            else
            {
                General.Error("Please run traning process to save traning data!");
            }
        }

        private void WriteToFile(String filename, String data)
        {
            using (StreamWriter sw = new StreamWriter(filename))
            {
                sw.Write(data);
            }
        }
    }
}
